import discord
from discord.ext import commands
Cog = commands.Cog

class Hi(Cog):
    def __init__(self, bot):
        super().__init__()

        self.bot = bot

    @Cog.listener()
    async def on_message(self, message):
        if message.guild is None and message.author.id != 663519590793347104:
            c = self.bot.get_channel(742518873303023707)

            if c is None:
                # We're not HTBote :D
                return

            prefixes = await self.bot.get_prefix(message)
            if not isinstance(prefixes, list):
                prefixes = []
            if any(message.content.startswith(i) for i in prefixes):
                # Ignore commands
                return

            embed = discord.Embed(description=message.content)
            if message.embeds:
                data = message.embeds[0]
                if data.type == 'image':
                    embed.set_image(url=data.url)

            if message.attachments:
                file_ = message.attachments[0]
                if file_.url.lower().endswith(('png', 'jpeg', 'jpg', 'gif')):
                    embed.set_image(url=file_.url)
                else:
                    embed.add_field(
                        name='Attachment',
                        value=f'[{file_.filename}]({file_.url})',
                        inline=False
                    )

            embed.set_author(name=message.author.display_name,
                             icon_url=message.author.avatar_url_as(format='png'))
            embed.timestamp = message.created_at

            await c.send(embed=embed)


def setup(bot):
    bot.add_cog(Hi(bot))
