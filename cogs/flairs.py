from discord.ext import commands
import discord
Cog = commands.Cog

import asyncio
import os


FLAIRS = {
    741770822896844891: { # Testing Server
        'Test Flairs': {
            'Hey Look a Role': 742528354422620171,
            'Hey Look another Role': 742529100102500372,
        },
    },
    406839907093446667: { # Main Server
        'School News': {
            'school news': 742527469634060391,
        },
        'Bot News': {
            'bot news': 742527731727728661,
        },
        'Minecraft Java User': {
            'java': 742527852288671836,
        },
        'Caden': {
            'caden': 742528062100602931,
        },
        'Ben': {
            'ben': 742528196687429734,
        },
    }
}


class Flairs(Cog):
    def __init__(self, bot):
        self.bot = bot

    async def safe_delete(self, ctx, after=5):
        async def _delete():
            await asyncio.sleep(after)
            if ctx.me.guild_permissions.manage_messages:
                await ctx.message.delete()

        asyncio.ensure_future(_delete(), loop=self.bot.loop)

    @commands.command()
    @commands.guild_only()
    async def listids(self, ctx):
        '''Get all of the IDs for the current server'''
        data = 'Your ID: {}\n\n'.format(ctx.author.id)

        data += 'Text Channel IDs:\n'
        for c in ctx.guild.channels:
            if isinstance(c, discord.TextChannel):
                data += '{}: {}\n'.format(c.name, c.id)

        data += '\nVoice Channel IDs:\n'
        for c in ctx.guild.channels:
            if isinstance(c, discord.VoiceChannel):
                data += '{}: {}\n'.format(c.name, c.id)

        data += '\nRole IDs:\n'
        for r in ctx.guild.roles:
            data += '{}: {}\n'.format(r.name, r.id)

        data += '\nUser IDs:\n'
        if ctx.guild.large:
            await self.bot.request_offline_members(ctx.guild)
        for msg in ctx.guild.members:
            data += '{}: {}\n'.format(msg.name, msg.id)

        filename = '{}-ids-all.txt'.format("".join([x if x.isalnum() else "_" for x in ctx.guild.name]))

        with open(filename, 'wb') as ids_file:
            ids_file.write(data.encode('utf-8'))

        await ctx.send(':mailbox_with_mail:')
        with open(filename, 'rb') as ids_file:
            await ctx.author.send(file=discord.File(ids_file))

        os.remove(filename)

    @commands.command(aliases=['listflairs'])
    @commands.guild_only()
    async def flairs(self, ctx):
        """List the available flairs for this server"""
        embed = discord.Embed(title='Available flairs:',
                              color=ctx.me.color.value)
        flairs = FLAIRS.get(ctx.guild.id)
        if flairs is None or len(flairs) == 0:
            embed.add_field(name='Error', value='No flairs setup')
        else:
            embed.description = f'*Use `{ctx.prefix}fclear [category '\
                                 '(optional)]` to remove your flairs.*'

            for f in flairs:
                embed.add_field(
                    name=f'*{f}*',
                    value='\n'.join(f'`{ctx.prefix}f {i}`' for i in flairs[f]),
                    inline=True)
        embed.set_footer(text=f'Requested by {ctx.author}')

        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def fclear(self, ctx, *, flair: str=''):
        """Remove all your flairs for this server"""
        await self.safe_delete(ctx)

        flairs = FLAIRS.get(ctx.guild.id)
        if flairs is None or len(flairs) == 0:
            return await ctx.send('No flairs setup', delete_after=5)

        if not ctx.me.guild_permissions.manage_roles:
            return await ctx.send('I don\'t have `Manage Roles` permission.',
                                  delete_after=5)

        if flair:
            for category in flairs:
                if category.lower() == flair.lower():
                    break
            else:
                return await ctx.send('No category found with that name.',
                                      delete_after=5)

            to_remove = []
            for f in flairs[category]:
                id_ = flairs[category][f]
                if isinstance(id_, tuple):
                    id_ = id_[0]
                role = discord.utils.get(ctx.guild.roles,
                                         id=id_)
                if role is not None:
                    to_remove.append(role)

            await ctx.author.remove_roles(*to_remove)

            return await ctx.send(f'All "{category}" flairs have been removed.',
                                  delete_after=5)
        else:
            to_remove = []

            for category in flairs:
                for f in flairs[category]:
                    role = discord.utils.get(ctx.guild.roles,
                                             id=flairs[category][f])
                    if role is not None:
                        to_remove.append(role)

            await ctx.author.remove_roles(*to_remove)

            return await ctx.send('All your flairs have been removed.',
                                  delete_after=5)

    @commands.command()
    @commands.guild_only()
    async def f_remove_all(self, ctx, *, flair: str):
        if not ctx.channel.permissions_for(ctx.author).manage_roles:
            await self.safe_delete(ctx)
            return

        flairs = FLAIRS.get(ctx.guild.id)
        if flairs is None or len(flairs) == 0:
            return await ctx.send('No flairs setup')

        if not ctx.me.guild_permissions.manage_roles:
            return await ctx.send('I don\'t have `Manage Roles` permission.')

        flat_flairs = [(i, flairs[category][i]) for category in flairs for i in flairs[category]]
        for flair_name, flair_id in flat_flairs:
            if isinstance(flair_id, tuple):
                if flair.lower() in [i.lower() for i in flair_id[1]]:
                    flair_id = flair_id[0]
                    break
            if flair_name.lower() == flair.lower():
                if isinstance(flair_id, tuple):
                    flair_id = flair_id[0]
                break
        else:
            return await ctx.send('No flair found with that name.')

        role = discord.utils.get(ctx.guild.roles, id=flair_id)

        if role is None:
            return await ctx.send(f'Failed to locate the role with id {flair_id}. Please check the configuration.')

        if ctx.guild.large:
            await ctx.bot.request_offline_members(ctx.guild)
        for member in ctx.guild.members:
            if role in member.roles:
                await member.remove_roles(role)

        return await ctx.send(f'The flair "{flair_name}" has been removed from every member.')

    @commands.command(aliases=['flair'])
    @commands.guild_only()
    async def f(self, ctx, *, flair: str=''):
        """Get a flair"""
        await self.safe_delete(ctx)

        flairs = FLAIRS.get(ctx.guild.id)
        if flairs is None or len(flairs) == 0:
            return await ctx.send('No flairs setup', delete_after=5)

        if not flair:
            return await ctx.send(f'I need to know what flair you want.. Use `{ctx.prefix}flairs` to list them all.',
                                  delete_after=5)

        if not ctx.me.guild_permissions.manage_roles:
            return await ctx.send('I don\'t have `Manage Roles` permission.', delete_after=5)

        flat_flairs = [(i, flairs[category][i], category) for category in flairs for i in flairs[category]]
        for flair_name, flair_id, category in flat_flairs:
            if isinstance(flair_id, tuple):
                if flair.lower() in [i.lower() for i in flair_id[1]]:
                    flair_id = flair_id[0]
                    break
            if flair.lower() == flair_name.lower():
                if isinstance(flair_id, tuple):
                    flair_id = flair_id[0]
                break
        else:
            return await ctx.send('No such flair available with that name.', delete_after=5)

        to_remove = []
        to_add = None
        for f in flairs[category]:
            if isinstance(flairs[category][f], tuple):
                id_ = flairs[category][f][0]
            else:
                id_ = flairs[category][f]
            role = discord.utils.get(ctx.guild.roles, id=id_)
            if id_ == flair_id:
                if role is None:
                    return await ctx.send('The flairs have be configured incorrectly; this flair is unavailable.',
                                          delete_after=5)
                to_add = role
            elif role is not None:
                to_remove.append(role)

        await ctx.author.remove_roles(*to_remove)
        await ctx.author.add_roles(to_add)

        return await ctx.send('You have been given the requested flair!', delete_after=5)


def setup(bot):
    bot.add_cog(Flairs(bot))
